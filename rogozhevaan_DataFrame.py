from pyspark.sql import SparkSession
from pyspark.sql.types import StructType,StructField,IntegerType,StringType
import pyspark.sql.functions as f

spark = SparkSession.builder.appName('rogozhevaan_DataFrame').master('yarn').getOrCreate()

n = 10  # number of partitions
struct_edg = StructType(fields = [StructField('out', IntegerType(), False),
                              StructField('in', IntegerType(), False)])
edges = spark.read.csv('/data/twitter/twitter_sample.txt', sep = '\t', schema = struct_edg)
edges = edges.repartition(n)

x = 12
x_end = 34
struct_path = StructType(fields = [StructField('end', IntegerType(), False),
                                    StructField('path', StringType(), False)])
paths = spark.createDataFrame([[x, str(x)]], schema = struct_path)
paths = paths.repartition(n)

N = 100
for _ in range(N):
    paths = paths.join(edges, paths['end'] == edges['in'], 'inner')
    paths = paths.select(f.col('out').alias('end'), f.concat_ws(',', paths['path'], paths['out']).alias('path'))
    count = paths.filter(paths['end'] == x_end).count()
    if count > 0:
        break
        
x_end, path = paths.filter(paths['end'] == x_end).first()
print(path)
